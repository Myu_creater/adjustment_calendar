import './App.css';
import {useState} from 'react'

import { initializeApp } from "firebase/app";
import { getFirestore, collection, addDoc} from "firebase/firestore";
// import onClickFirebase from './component/firebase/init';
function App() {

  const [nameText,setNameText] = useState({})

  const onChangeName = (e) =>{
    setNameText(e.target.value)
    console.log(nameText)
  }

  const submitDatabase = () =>{
    const result = {
      name: nameText
    }
    onClickFirebase(result)
    console.log("submit on app")
  }


const firebaseConfig = {
  apiKey: process.env.REACT_APP_APIKEY,
  authDomain: process.env.REACT_APP_AUTHDOMAIN,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket:process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID
};

initializeApp(firebaseConfig)


let db = getFirestore()
console.log(db)

const onClickFirebase = async (obj) =>{
  console.log("ok")
  try {
    const docRef = await addDoc(collection(db, "users"), obj);
    console.log("Document written with ID: ", docRef.id);
    console.log("submit on firebase")
  } catch (e) {
    console.error("Error adding document: ", e);
  }
}


  return (
    <div className="App">
      <input type="text" value={nameText} onChange={onChangeName} />
      <button onClick={submitDatabase}>データを追加</button>
    </div>
  );
}

export default App;
